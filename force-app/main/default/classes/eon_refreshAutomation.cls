public with sharing class eon_refreshAutomation {
    


    public static void refreshSteps() 
    {
//        refreshSandbox();
//        userDeactivation();
//        removeProdURLs();
//        removeScheduledReports();

    }





// wirklich ektuell benötigt? Refresh kann mit 1-2 klicks getriggert werden (Hier gute Anleitung https://manzanit0.github.io/salesforce/2018/06/27/aquamon-part1.html)
    public static void refreshSandbox() 
    {
        //before Callout: establish an OAuth Connection
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('URL/services/data/v51.0/tooling/sobjects/SandboxInfo/{id}');
        request.setMethod('PATCH');
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) 
        {
            //TODO   
        }
        else {
            try {
                
            } catch (Exception e) {
                eon_HandleCustomException.LogException(e);
            }    
        }
    }


    public static void userDeactivation()
    {
        List<User> users = [Select id, isActive from user where isActive = true and usertype = 'Standard' and id not in ('0050Y000003lNR0QAM')];
        
        for(User u: users)
        {
            u.isActive = false;
        }
        system.debug(users.size());
        update users;
    }


    public static void removeProdURLs()
    {
        //Custom Settings - WORKS!
        eon_test__c c1 = eon_test__c.getInstance('test2');  //test2 for "Integration Administration" OR "Bit2Win Integration - outbound"
        System.debug(c1.endpoint__c);
        c1.endpoint__c = 'www.bayer04.de';
        update c1;
        System.debug(c1.endpoint__c);


        //Custom Metadata Type
        //"Next Action Setting" < no URLs in there?


        //Custom Labels - Not possible
        //https://trailblazer.salesforce.com/ideaView?id=08730000000DgpGAAS
        // https://github.com/financialforcedev/apex-mdapi/blob/master/apex-mdapi/src/classes/MetadataService.cls


        //Outbound Messages - Deploy with Metadata API
        //https://help.salesforce.com/articleView?id=000315580&mode=1&type=1


        //Auth. Provider - over Metadata API
        List<AuthProvider> provList = [SELECT Id, AuthorizeUrl, TokenUrl FROM AuthProvider WHERE FriendlyName LIKE '%test%'];
        System.debug(provList);
        for (AuthProvider p : provList)
        {
            p.AuthorizeUrl = 'https://notavalideurl.com/services/oauth2/authorize';
            p.TokenUrl = 'https://notavalideurl.com/services/oauth2/token';
        }
        System.debug(provlist);
        //update provList; < not allowed, über Metadata API zu deployen 


        //External Data Source - URL Update via Metadata API
        //Connected Apps - URL Update via Metadata API (Consumer Key ist aus xml auslesbar)
        //Remote Site Settings - URL Update via Metadata API
    }


    //Remove the Schedule from Reports -- Worked!
    private static void removeScheduledReports()
    {
        List<CronTrigger> reportNotificationJobs = [SELECT Id FROM CronTrigger WHERE CronJobDetail.JobType = 'A'];
        if (!reportNotificationJobs.isEmpty()) 
        {
            for (CronTrigger cronTriggerItem : reportNotificationJobs) 
            {
            System.abortJob(cronTriggerItem.Id);
            }
        }
    }

    //Chatbot deletion - via Metadata API
    private static void deleteChatbots()
    {
        
    //Code inside eache VF-Page which has to be edited after each refresh    
    /* 

    <apex:page standardStylesheets="false" showHeader="false" >
    </apex:page>     

    DA_Chat_SnapIn
    ChatInnogy_SnapIn
    Chat_Solar_SnapIn
    Chat_Core_SME_SnapIn
    Chat_SmartPlus_SnapIn

    */
    }


}